from typing import List, Generator, Tuple

from exceptions import GeomBoundsError


class Point:
    """2d point on Earth's surface"""

    def __init__(self, x: float, y: float):
        if -180.0 >= x >= 180.0 or -90.0 >= y >= 90.0:
            m = f"Point({x}, {y}) out of bounds (-180, 180), (-90, 90)"
            raise GeomBoundsError(m)
        self._x, self._y = x, y

    def __repr__(self):
        return f"<Point({self._x}, {self._y})>"

    def __eq__(self, other: "Point"):
        return self.x == other.x and self.y == other.y

    @property
    def x(self):
        """Read only point, so don't allow setting property"""
        return self._x

    @property
    def y(self):
        """Read only point, so don't allow setting property"""
        return self._y

class LineString:
    def __init__(self, *points):
        self._points = list(points)


class BBox:
    """Simple Bounding Box"""
    def __init__(self, xmin: float, xmax: float, ymin: float, ymax: float):
        self._xmin = xmin
        self._xmax = xmax
        self._ymin = ymin
        self._ymax = ymax
    
    def __repr__(self):
        return f"<BBox({self._xmin}, {self._xmax}, {self._ymin}, {self._ymax})>"

    def __eq__(self, other: "BBox") -> bool:
        return all(a == b for a, b in zip(self.corners, other.corners))

    def contains(self, point: Point):
        """determine if a bounding box contains a point"""
        xc = self._xmin <= point.x <= self._xmax
        yc = self._ymin <= point.y <= self._ymax
        return xc and yc

    @property
    def corners(self):
        return (
            Point(self._xmin, self._ymin),
            Point(self._xmax, self._ymin),
            Point(self._xmax, self._ymax),
            Point(self._xmin, self._ymax)
        )

    def overlaps(self, other: "BBox") -> bool:
        raise NotImplementedError()

    @classmethod
    def merge(cls, a: "BBox", b: "BBox"):
        # if a.overlaps(b):
        xmin = min(a.xmin, b.xmin)
        xmax = max(a.xmax, b.xmax)
        ymin = min(a.ymin, b.ymin)
        ymax = max(a.ymax, b.ymax)
        return cls(xmin, xmax, ymin, ymax)

    @property
    def xmin(self):
        return self._xmin

    @property
    def xmax(self):
        return self._xmax

    @property
    def ymin(self):
        return self._ymin

    @property
    def ymax(self):
        return self._ymax

