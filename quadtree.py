"""
implementation of a quadtree spatial index
"""
from typing import List, Generator, Tuple
from exceptions import InsertError
from geometry import BBox, Point


DEBUG = True

class QuadTree:
    """
    class QuadTree is an implementation of a QuadTree spatial index.
    Each node will either have 4 children QuadTrees or a list of points.
    Nodes will not have both; therefore, the lowest node will have <= _node_cap
    points.
    """

    def __init__(self, bbox: BBox, *, node_cap=4, parent=None):
        self._node_cap = node_cap
        self._bb = bbox
        self._parent = parent
        # child nodes
        self._ne: QuadTree = None
        self._nw: QuadTree = None
        self._se: QuadTree = None
        self._sw: QuadTree = None
        self._points: List[Point] = list()

    def __repr__(self):
        children = ", ".join(repr(c) for c in self.children)
        return f"<QuadTree({self._bb}, children=[{children}])>"

    @property
    def bbox(self):
        """the bounding box of a Tree node"""
        return self._bb

    @property
    def children(self) -> Tuple["QuadTree", Point]:
        """all child nodes"""
        if self._ne:
            return self._ne, self._nw, self._se, self._sw
        return tuple(self._points)

    def insert(self, point: Point) -> bool:
        """insert a point into a quadtree"""
        if not self._bb.contains(point):
            return False
        if len(self._points) < self._node_cap:
            self._points.append(point)
            return True
        if not self._ne:
            self.subdivide()
            self._node_cap = 0
        nodes = self._ne, self._nw, self._se, self._sw
        return any(n.insert(point) for n in nodes)

    def subdivide(self):
        """divide into 4 quadrants an redistribute points"""
        xmid = (self._bb.xmax + self._bb.xmin) / 2
        ymid = (self._bb.ymax + self._bb.ymin) / 2
        ne = QuadTree(
            BBox(
                xmid, self._bb.xmax,
                ymid, self._bb.ymax
            ),
            node_cap=self._node_cap, parent=self
        )
        nw = QuadTree(
            BBox(
                self._bb.xmin, xmid,
                ymid, self._bb.ymax
            ),
            node_cap=self._node_cap, parent=self
        )
        se = QuadTree(
            BBox(
                xmid, self._bb.xmax,
                self._bb.ymin, ymid
            ),
            node_cap=self._node_cap, parent=self
        )
        sw = QuadTree(
            BBox(
                self._bb.xmin, xmid,
                self._bb.ymin, ymid
            ),
            node_cap=self._node_cap, parent=self
        )
        for point in self._points:
            if not any(q.insert(point) for q in [ne, nw, se, sw]):
                raise InsertError(f"could not insert {point}")
        self._points = list()
        self._ne = ne
        self._nw = nw
        self._se = se
        self._sw = sw
        return self

    @property
    def points(self) -> Generator[Point, None, None]:
        """all child points in a tree"""
        for child in self.children:
            if isinstance(child, QuadTree):
                yield from child.points
            else:
                yield child

    def query(self, bbox: BBox) -> Generator[Point, None, None]:
        """query by a bounding box"""
        if self._points:
            for point in self._points:
                if bbox.contains(point):
                    yield point
        else:
            for tree in [self._ne, self._nw, self._se, self._sw]:
                if tree:
                    yield from tree.query(bbox)

    def terminal(self) -> Generator[BBox, None, None]:
        """lowest level nodes with only points as children"""
        if len(self._points) > 0:
            yield self._bb
        else:
            for child in [self._ne, self._nw, self._se, self._sw]:
                if child:
                    yield from child.terminal()

def pprint_tree(tree: QuadTree):
    """pretty print a tree"""
    for node in tree.children:
        if isinstance(node, QuadTree):
            print(f"QuadTree(bbox={node.bbox})")
            pprint_tree(node)
        else:
            print(f"\t{node}")


def main():
    """entrypoint"""
    import random
    import sys
    import time
    node_cap = 4
    if len(sys.argv) > 1:
        node_cap = int(sys.argv[1])

    bbox = BBox(-180, 180, -90, 90)
    tree = QuadTree(bbox, node_cap=node_cap)
    random.seed(42 if DEBUG else int(time.time()))
    for _ in range(1_000_000):
        point = random.random() * 180.0, random.random() * 90.0
        tree.insert(Point(*point))
    box = BBox.merge(BBox(45.0, 90.0, 22.5, 45.0), BBox(0, 45.0, 22.5, 45.0))
    found = tree.query(box)
    return tree, found, tree.terminal()


# hack for getting stuff from `python -i quadtree.py`
STUFF = None


if __name__ == '__main__':
    STUFF = main()
