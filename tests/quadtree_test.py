"""unit tests for QuadTree"""
import unittest
import context as _

from geometry import Point, BBox
from quadtree import QuadTree

class TestQuadTree(unittest.TestCase):
    """
    Tests for the quadtree spatial index implementation
    """

    def test_one_level(self):
        """
        a quadtree with all points inside the bounding box and
        # nodes == node_cap
        """
        box = BBox(0, 10, 0, 10)
        tree = QuadTree(box, node_cap=4)
        points = (
            Point(1, 1),
            Point(2, 5),
            Point(9, 1),
            Point(10, 10)
        )
        for point in points:
            tree.insert(point)
        self.assertTupleEqual(points, tuple(tree.points))
        self.assertEqual(box, tree.bbox)
        term = list(tree.terminal())
        self.assertEqual(box, term[0])
        self.assertTrue(len(term) == 1)

    def test_out_of_bounds_insert(self):
        """inserting a point outside of the bbox should not work"""
        box = BBox(0, 1, 0, 1)
        point = Point(1, 1.1)
        tree = QuadTree(box)
        self.assertFalse(tree.insert(point))

    def test_one_subdivision(self):
        """inserting > node_cap should cause a subdivision and redistribution"""
        box = BBox(0, 10, 0, 10)
        tree = QuadTree(box, node_cap=4)
        points = (
            Point(1, 1),
            Point(2, 5),
            Point(9, 1),
            Point(10, 10),
            Point(3, 2),
        )
        for point in points:
            tree.insert(point)
        i = 0
        for point in tree.points:
            i += 1
            self.assertIn(point, points)
        self.assertEqual(i, len(points))
        self.assertEqual(box, tree.bbox)
        term = list(tree.terminal())
        self.assertEqual(len(term), 4)

if __name__ == '__main__':
    unittest.main()
