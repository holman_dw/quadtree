# Python Spatial Indexes

This project aims to implement spatial indexes for use in python
projects. All will be initially implemented in python 3.6+. After
initial implementations and sufficient tests are written, some
portions may be rewritten in a lower level language (C/rust) for
better performance. As pieces are rewritten, the initial interfaces
will be modified to use the lower level implementations.

# Tests

```
cd tests
python quadtree.py # assumes python is 3.6
```

# Contact

[@holman_dw](https://twitter.com/holman_dw) on twitter

# License
MIT (See `LICENSE`)

# TODO

* add support for other geometry types