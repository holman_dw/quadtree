
class GeomBoundsError(Exception):
    pass

class InsertError(Exception):
    pass